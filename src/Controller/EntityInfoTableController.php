<?php
namespace Drupal\entity_info_table\Controller;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\devel\DevelDumperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformHelpManager;
class EntityInfoTableController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel.dumper')
    );
  }

  public function render() {
    $headers = [
      $this->t('ID'),
      $this->t('Name'),
      $this->t('bundle'),
      $this->t('bundle label'),
      $this->t('Field name'),
      $this->t('Field type'),
      $this->t('field description'),
    ];

    $rows = [];

    foreach ($this->entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
      // Fail with an exception for non-fieldable entity types.
      $bundle_list = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);

      if ($entity_type_id == 'webform') {
//        $query = \Drupal::service('entity.query')->get('webform');
//        dpm($query);
      }
      if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
        //        dpm("Getting the base fields is not supported for entity type {$entity_type->getLabel()}.");
        continue;
      }
      foreach ($bundle_list as $bundle_key => $bundle_info) {
        foreach (\Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle_key) as $field_name => $field_definition) {
          if (!empty($field_definition->getTargetEntityTypeId())) {
            $bundleFields[$entity_type_id][$bundle_key][$field_name]['type'] = $field_definition->getType();
            $bundleFields[$entity_type_id][$bundle_key][$field_name]['label'] = $field_definition->getLabel();
            $row['id'] = [
              'data' => $entity_type->id(),
              'filter' => TRUE,
            ];
            $row['name'] = [
              'data' => $entity_type->getLabel(),
              'filter' => TRUE,
            ];
            $row['bundle'] = [
              'data' => $bundle_key,
              'filter' => TRUE,
            ];
            $row['bundle_label'] = [
              'data' => $bundle_info['label'],
              'filter' => false,
            ];
            $row['field_name'] = [
              'data' => $field_definition->getLabel(),
              'filter' => TRUE,
            ];
            $row['field_type'] = [
              'data' => $field_definition->getType(),
              'filter' => TRUE,
            ];
            $field_description = $field_definition->getFieldStorageDefinition()->getDescription() ? $field_definition->getFieldStorageDefinition()->getDescription() : NULL;
//            dpm($field_description);
            $row['field_description'] = [
              'data' => $field_description,
              'filter' => TRUE,
            ];
            $rows[] = $row;
          }
        }
      }
    }
    ksort($rows);

    $output['entities'] = [
      '#type' => 'devel_table_filter',
      '#filter_label' => $this->t('Search'),
      '#filter_placeholder' => $this->t('Enter entity type id, provider or class'),
      '#filter_description' => $this->t('Enter a part of the entity type id, provider or class to filter by.'),
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No entity types found.'),
      '#sticky' => TRUE,
      '#attributes' => [
        'class' => ['devel-entity-type-list'],
      ],
    ];

    return $output;
  }
}