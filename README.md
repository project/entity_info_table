# Entity Information Table

Render entity info in table. easily to copy/paste to excel. It's tools used by project manager

[project page](https://www.drupal.org/project/entity_info_table).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_info_table).

## Table of contents (optional)

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers

## Requirements (required)

This module requires the following modules:

- [Devel](https://www.drupal.org/project/devel)


## Installation (required, unless a separate INSTALL.md is provided)

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration (required)

1. Enable the module at Administration > Extend.
1. goto /admin/reports/entity-info-table .

## Troubleshooting (optional)



## Maintainers (optional)

- Terry Zhang - [dries](https://www.drupal.org/u/zterry95)
